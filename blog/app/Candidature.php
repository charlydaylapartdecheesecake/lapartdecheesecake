<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidature extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function offreEmploi(){
        return $this->belongsTo('App\offre_emploi', 'offre_id');
    }
}