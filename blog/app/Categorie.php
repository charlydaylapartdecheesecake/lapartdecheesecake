<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    public function fullString(){
    	return $this->type." ".$this->poste;
    }
}
