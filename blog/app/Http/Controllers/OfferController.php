<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
class OfferController extends Controller
{
    public function displayCreateForm(){
    	return view('offer/create', ['categories' => \App\Categorie::all()]);
    }

    public function registerOffer(){
        $cate_id = \App\Categorie::where("type", "=", request("cate_type"))->where("poste", "=", request("cate_poste"))->first()->id;
    	\App\offre_emploi::Create([
            'intitule' => request('intitule'),
            'description' => request('description'),
            'duree' => request('duree'),
            'adresse' => request('adresse'),
            'user_id' => \Auth::user()->id,
            'cate_id' => $cate_id
        ]);
    	return redirect('/');
    }
    public function voir_offre($id){
        $offre = \App\offre_emploi::find($id);
        abort_if(!$offre,404);
        if(is_null(\Auth::user()))
            $route = route("login");
        else
            $route = route("candidater", ["id"=>$id]);
        return view('offer/show',['offre' => $offre, "route"=>$route]);
    }
    public function candidate($id){
        $offre = \App\offre_emploi::find($id);
        abort_if(!$offre,404);
        return view('offer/candidater',['offre' => $offre]);
    }

    public function listes_offres(){
        $offres = \App\offre_emploi::all();
        return view('offer/listes_offres', compact('offres'));
    }

    public function posteParCategorie($type){
        $cates = \App\Categorie::where('type', '=', $type)->get();
        return view('offer/postes', compact('cates'));
    }

    public function asJSON(){
        $offres = array();
        foreach (\App\offre_emploi::all() as $offre) {
            $route = route('show_offre',array('id'=>$offre->id));
            $cate = $offre->categorie()->fullString();
            $offre->categorie = $cate;
            $offre->employeur = $offre->user()->getFullName();
            $offre->html = <<<END
                <div class="list-groupe ml-4 mr-4">
                <a href="$route" class="list-group-item list-group-item-action list-group-item-primary" >
                    <div class="d-flex w-100 justify-content-between">
                        <h2 class="mb-1" >$offre->intitule</h2>
                        <small class="mb-1" >$cate</small>
                    </div>
                    <p class="mb-1"> Description :$offre->description</p>
                    <small class="mb-1"> Lieu  :$offre->adresse</small>

                </a>
                <br>
            </div>
END;
            $offres[] = $offre;
        }
        return json_encode($offres);
    }
}
