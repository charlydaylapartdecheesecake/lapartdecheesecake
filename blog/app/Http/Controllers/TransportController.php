<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransportController extends Controller
{
    public function besoinTransport($id){
        //abort_if(request()->has('vehicule'),404);
        request()->validate(
            [
                'vehicule' =>'required|in:on,off'//,
                //'typeVehicule'=>'required|in:male,female'
            ]
            );
        $candidature  = new \App\Candidature();
        $candidature->offre_id = $id;
        $candidature->besoinTransport = (request('vehicule') === 'on') ? true : false ;
        $candidature->candidat_id = \Auth::user()->id;
        $candidature->save();
        if(request('vehicule') === 'on'){
            request()->validate(
                [
                    'typeVehicule' =>'required|in:normal,adapte',
                    'depart' => 'string|max:191'
                ]
                );
                $besoin = new \App\BesoinTransport();
                $besoin->candidature_id = $candidature->id;
                $besoin->type_vehicule = request('typeVehicule');
                $besoin->lieu_depart = request('depart');
                $emploi =$candidature->offreEmploi()->get()->toArray();
                $besoin->lieu_arrivee = $emploi[0]["adresse"];
                $besoin->save();

        }
        return redirect()->route('listes_offres');
        
        
    }

    public function postulerAuBesoin($id){
        $offreTransport  = new \App\OffreTransport();
        $offreTransport->chauffeur_id= \Auth::user()->id;
        $offreTransport->save();

        $propo  = new \App\PropositionTransport();
        $propo->besoin_id= $id;
        $propo->offre_id= $offreTransport->id;
        $propo->save();

        $besoin  = \App\BesoinTransport::find($id);
        if($besoin->etat_besoin=="EnAttente"){
            $besoin->etat_besoin=="EnCours";
        }


        $besoins = \App\BesoinTransport::all();
        return $this->listes_besoin_transport();
    }

    public function listes_besoin_transport(){
        $besoins = \App\BesoinTransport::all();
        return view('transports/besoins_transports', compact('besoins'));
    }
}
