<?php

namespace App\Http\Controllers;


class UserController extends Controller
{
    public function listesUtilisateurs()
    {
        return view('user/listing', ['users' => \App\User::all()]);
    }

    public function listesCandidatures()
    {
        $candidatures = \Auth::user()->getCandidatures();
        return view('user/candidatures', ['candidatures' => $candidatures]);
    }

    public function listingOffresTransport()
    {
        $offres = \Auth::user()->getOffresTransport();
        return view('user/mes_offres', ['offres' => $offres]);
    }

    public function displayModif(){
        return view('user/modifier');
    }


    public function listeMesCandidatures(){
        $offres = array();
        foreach (\Auth::user()->getMesCandidatures() as $candidature) {
            $offres[] = $candidature->offreEmploi()->first();
        }
        return view('user/mes_candidatures', ['offres' => $offres]);
    }
}