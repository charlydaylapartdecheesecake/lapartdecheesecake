<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'first_name', 'adresse_perso', 'adresse_pro', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'admin'
    ];

    public function getCandidatures()
    {
        return \DB::table('candidatures')->join('offre_emplois', 'candidatures.offre_id', '=', 'offre_emplois.id')
            ->join('users', 'offre_emplois.user_id', '=', 'users.id')
            ->where('users.id', '=', $this->id)
            ->get();
    }

    public function getMesCandidatures()
    {
        return $this->hasMany('\App\Candidature', 'candidat_id', 'id')->get();
    }

    public function getFullName(){
        return $this->name." ".$this->first_name;
    }
}
