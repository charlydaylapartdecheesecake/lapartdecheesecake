<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class offre_emploi extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function categorie(){
    	return $this->hasOne('App\Categorie', 'id', 'cate_id')->first();
    }

    public function user(){
    	return $this->hasOne('App\User', 'id', 'user_id')->first();
    }
}
