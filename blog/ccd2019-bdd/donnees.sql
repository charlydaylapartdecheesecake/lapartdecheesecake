

INSERT INTO `categories` ( `type`,`poste`)
VALUES
       ('Administratif','assistant comptable'),
       ('Administratif',' comptable'),
       ('Administratif ',' secrétaire'),
       ('Administratif ',' standardiste'),

       ('Bâtiment/Travaux Publics ',' conducteur d\'engin'),
       ('Bâtiment/Travaux Publics ','manœuvre'),
       ('Bâtiment/Travaux Publics ',' maçon'),
       ('Bâtiment/Travaux Publics ',' électricien'),
       ('Commerce et vente ',' assistant commercial'),
       ('Commerce et vente',' commercial'),
       ('Commerce et vente',' manager'),
       ('Commerce et vente ',' vendeur polyvalent'),
       ('Logistique ',' magasinier'),
       ('Logistique',' préparateur de commandes'),
       ('Restauration et hôtellerie',' aide cuisinier'),
       ('Restauration et hôtellerie ',' cuisinier'),
       ('Restauration et hôtellerie ',' employé polyvalent'),
       ('Restauration et hôtellerie ',' serveur'),
       ('Transport ','cariste'),
       ('Transport',' chauffeur de bus'),
       ('Transport ',' conducteur poids lourd'),
       ('Transport ',' livreur');

INSERT INTO `users` ( `name`,`first_name`,`email`,`password`,`adresse_perso`)
VALUES
  ('Cassandre','Azer','Cassandre@gmail.com','1234','1ChezMoi'),
  ('Achille','Foul','Achille@gmail.com','1234','2ChezMoi'),
  ('Calypso','Wasul','Calypso@gmail.com','1234','3ChezMoi'),
  ('Bacchus','Noirot','Bacchus@gmail.com','1234','4ChezMoi'),
  ('Diane','Cos','Diane@gmail.com','1234','5ChezMoi'),
  ('Clark','Op','Clark@gmail.com','1234','6ChezMoi'),
  ('Helene','Palau','Helene@gmail.com','1234','7ChezMoi'),
  ('Jason','Vermande','Jason@gmail.com','1234','8ChezMoi'),
  ('Bruce','benamran','Bruce@gmail.com','1234','9ChezMoi'),
  ('Pénélope','benamran','Pénélope@gmail.com','1234','10ChezMoi'),
  ('Ariane','grande','Ariane@gmail.com','1234','11ChezMoi'),
  ('Lois','hop''Lois','Lois@gmail.com','1234','12ChezMoi');

INSERT INTO `offre_emplois` ( `intitule`,`description`,`duree`,`adresse`,`etat_offre`,`user_id`,`cate_id`)
VALUES
       ('Dev','Pour dut','10','Nancy','1','1','1'),
       ('Commerce','Pas grand chose','12','Metz','1','2','1'),
       ('Esport','metier?','8','ChezNoirot','1','3','2'),
       ('Directeur','Bien','5','Paris','1','1','1'),
       ('chef de projet','super','Berlin','1234','1','1','3');
