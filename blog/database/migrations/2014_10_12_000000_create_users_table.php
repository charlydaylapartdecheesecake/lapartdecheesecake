<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('first_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('adresse_perso')->nullable();
            $table->string('adresse_pro')->nullable();
            $table->boolean('admin')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
        DB::table('users')->insert(array('name'=>'Cassandre', 'first_name'=>'Azer!','email'=>'Cassandre@gmail.com','password'=>'','adresse_perso'=>'1ChezMoi'));
        DB::table('users')->insert(array('name'=>'Achille', 'first_name'=>'Foul!','email'=>'Achille@gmail.com','password'=>'','adresse_perso'=>'2ChezMoi'));
        DB::table('users')->insert(array('name'=>'Calypso', 'first_name'=>'Wasul!','email'=>'Calypso@gmail.com','password'=>'','adresse_perso'=>'3ChezMoi'));
        DB::table('users')->insert(array('name'=>'Bacchus', 'first_name'=>'Noirot!','email'=>'Bacchus@gmail.com','password'=>'','adresse_perso'=>'4ChezMoi'));
        DB::table('users')->insert(array('name'=>'Diane', 'first_name'=>'Cos!','email'=>'Diane@gmail.com','password'=>'','adresse_perso'=>'5ChezMoi'));
        DB::table('users')->insert(array('name'=>'Clark', 'first_name'=>'Op!','email'=>'Clark@gmail.com','password'=>'','adresse_perso'=>'6ChezMoi'));
        DB::table('users')->insert(array('name'=>'Helene', 'first_name'=>'Palau!','email'=>'Helene@gmail.com','password'=>'','adresse_perso'=>'7ChezMoi'));
        DB::table('users')->insert(array('name'=>'Jason', 'first_name'=>'Vermande!','email'=>'Jason@gmail.com','password'=>'','adresse_perso'=>'8ChezMoi'));
        DB::table('users')->insert(array('name'=>'Bruce', 'first_name'=>'benamran!','email'=>'Bruce@gmail.com','password'=>'','adresse_perso'=>'9ChezMoi'));
        DB::table('users')->insert(array('name'=>'Pénélope', 'first_name'=>'benamran!','email'=>'Pénélope@gmail.com','password'=>'','adresse_perso'=>'10ChezMoi'));
        DB::table('users')->insert(array('name'=>'Ariane', 'first_name'=>'grande!','email'=>'Ariane@gmail.com','password'=>'','adresse_perso'=>'11ChezMoi'));
        DB::table('users')->insert(array('name'=>'Lois', 'first_name'=>'Lois!','email'=>'Lois@gmail.com','password'=>'','adresse_perso'=>'12ChezMoi'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
