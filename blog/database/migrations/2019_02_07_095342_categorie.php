<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Categorie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('poste');
            $table->rememberToken();
            $table->timestamps();
        });
        DB::table('categories')->insert(array('type'=>'Administratif', 'poste'=>'assistant comptable'));
        DB::table('categories')->insert(array('type'=>'Administratif', 'poste'=>'comptable'));
        DB::table('categories')->insert(array('type'=>'Administratif', 'poste'=>'secrétaire'));
        DB::table('categories')->insert(array('type'=>'Administratif', 'poste'=>'standardiste'));
        DB::table('categories')->insert(array('type'=>'Bâtiment/Travaux Publics', 'poste'=>'conducteur d\'engin'));
        DB::table('categories')->insert(array('type'=>'Bâtiment/Travaux Publics', 'poste'=>'manoeuvre'));
        DB::table('categories')->insert(array('type'=>'Bâtiment/Travaux Publics', 'poste'=>'maçon'));
        DB::table('categories')->insert(array('type'=>'Bâtiment/Travaux Publics', 'poste'=>'électricien'));
        DB::table('categories')->insert(array('type'=>'Commerce et vente', 'poste'=>'assistant commercial'));
        DB::table('categories')->insert(array('type'=>'Commerce et vente', 'poste'=>' commercial'));
        DB::table('categories')->insert(array('type'=>'Commerce et vente', 'poste'=>' manager'));
        DB::table('categories')->insert(array('type'=>'Commerce et vente', 'poste'=>' vendeur polyvalent'));
        DB::table('categories')->insert(array('type'=>'Logistique', 'poste'=>' magasinier'));
        DB::table('categories')->insert(array('type'=>'Logistique', 'poste'=>' préparateur de commandes'));
        DB::table('categories')->insert(array('type'=>'Restauration et hôtellerie', 'poste'=>' aide cuisinier'));
        DB::table('categories')->insert(array('type'=>'Restauration et hôtellerie', 'poste'=>' cuisinier'));
        DB::table('categories')->insert(array('type'=>'Restauration et hôtellerie', 'poste'=>' employé polyvalent'));
        DB::table('categories')->insert(array('type'=>'Restauration et hôtellerie', 'poste'=>' serveur'));
        DB::table('categories')->insert(array('type'=>'Transport', 'poste'=>' cariste'));
        DB::table('categories')->insert(array('type'=>'Transport', 'poste'=>' chauffeur de bus'));
        DB::table('categories')->insert(array('type'=>'Transport', 'poste'=>' conducteur poids lourd'));
        DB::table('categories')->insert(array('type'=>'Transport', 'poste'=>' livreur'));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorie');
    }
}
