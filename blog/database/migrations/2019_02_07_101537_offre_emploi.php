<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OffreEmploi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offre_emplois', function (Blueprint $table) {
            $table->increments('id');
            $table->string('intitule');
            $table->longtext('description');
            $table->string('duree');
            $table->string('adresse');
            $table->boolean('etat_offre')->default(1);
            $table->rememberToken();
            $table->timestamps();

            $table->unsignedInteger('user_id');
            $table->unsignedInteger('cate_id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->foreign('cate_id')->references('id')->on('categories')->onDelete('cascade');;
        });
        DB::table('offre_emplois')->insert(array('intitule'=>'Dev', 'description'=>'Pour dut!','duree'=>'10','adresse'=>'Nancy','etat_offre'=>'1','user_id'=>'1','cate_id'=>'1'));
        DB::table('offre_emplois')->insert(array('intitule'=>'Commerce', 'description'=>'Pas grand chose','duree'=>'10','adresse'=>'Metz','etat_offre'=>'1','user_id'=>'2','cate_id'=>'3'));
        DB::table('offre_emplois')->insert(array('intitule'=>'Esport', 'description'=>'metier','duree'=>'10','adresse'=>'Amsterdam','etat_offre'=>'1','user_id'=>'2','cate_id'=>'1'));
        DB::table('offre_emplois')->insert(array('intitule'=>'Directeur', 'description'=>'Bien','duree'=>'10','adresse'=>'Paris','etat_offre'=>'1','user_id'=>'3','cate_id'=>'2'));
        DB::table('offre_emplois')->insert(array('intitule'=>'chef de projet', 'description'=>'super','duree'=>'10','adresse'=>'Berlin','etat_offre'=>'1','user_id'=>'3','cate_id'=>'1'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offre_emplois');
    }
}
