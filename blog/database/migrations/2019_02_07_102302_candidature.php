<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Candidature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidatures', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('candidat_id');
            $table->foreign('candidat_id')->references('id')->on('users')->onDelete('cascade');;

            $table->boolean('besoinTransport');
            $table->string('etat_candidature')->default('attente'); //attente/retenue/refusée
            $table->unsignedInteger('offre_id');
            $table->foreign('offre_id')->references('id')->on('offre_emplois')->onDelete('cascade');;
            $table->rememberToken();
            $table->timestamps();
        });
        DB::table('candidatures')->insert(array('candidat_id' => 1, 'besoinTransport' => false, 'offre_id' => 1));
        DB::table('candidatures')->insert(array('candidat_id' => 1, 'besoinTransport' => false, 'offre_id' => 1));
        DB::table('candidatures')->insert(array('candidat_id' => 2, 'besoinTransport' => true, 'offre_id' => 1));
        DB::table('candidatures')->insert(array('candidat_id' => 3, 'besoinTransport' => true, 'offre_id' => 1));
        DB::table('candidatures')->insert(array('candidat_id' => 3, 'besoinTransport' => true, 'offre_id' => 2));
        DB::table('candidatures')->insert(array('candidat_id' => 3, 'besoinTransport' => true, 'offre_id' => 3));
        DB::table('candidatures')->insert(array('candidat_id' => 4, 'besoinTransport' => false, 'offre_id' => 3));
        DB::table('candidatures')->insert(array('candidat_id' => 4, 'besoinTransport' => false, 'offre_id' => 2));
        DB::table('candidatures')->insert(array('candidat_id' => 4, 'besoinTransport' => false, 'offre_id' => 1));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidatures');
    }
}
