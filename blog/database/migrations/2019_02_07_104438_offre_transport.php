<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OffreTransport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offre_transports', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('chauffeur_id');
            $table->foreign('chauffeur_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('offre_acceptee')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        DB::table('offre_transports')->insert(array('chauffeur_id' => 5, 'offre_acceptee' => true));
        DB::table('offre_transports')->insert(array('chauffeur_id' => 3, 'offre_acceptee' => false));
        DB::table('offre_transports')->insert(array('chauffeur_id' => 9, 'offre_acceptee' => false));
        DB::table('offre_transports')->insert(array('chauffeur_id' => 6, 'offre_acceptee' => false));
        DB::table('offre_transports')->insert(array('chauffeur_id' => 7, 'offre_acceptee' => false));
        DB::table('offre_transports')->insert(array('chauffeur_id' => 8, 'offre_acceptee' => false));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offre_transports');
    }
}
