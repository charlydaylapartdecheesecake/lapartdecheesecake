<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BesoinTransport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('besoin_transports', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('candidature_id');
            $table->foreign('candidature_id')->references('id')->on('candidatures')->onDelete('cascade');
            $table->string('lieu_depart');
            $table->string('lieu_arrivee');
            $table->string('etat_besoin')->default('EnAttente');
            $table->string('type_vehicule');
            $table->rememberToken();
            $table->timestamps();
        });
        DB::table('besoin_transports')->insert(array('candidature_id' => 3, 'lieu_depart' => '2ChezMoi', 'lieu_arrivee' => 'Nancy', 'etat_besoin' => 'Complet', 'type_vehicule' => 'Adapte'));
        DB::table('besoin_transports')->insert(array('candidature_id' => 4, 'lieu_depart' => '3ChezMoi', 'lieu_arrivee' => 'Nancy', 'etat_besoin' => 'EnAttente', 'type_vehicule' => 'Normal'));
        DB::table('besoin_transports')->insert(array('candidature_id' => 5, 'lieu_depart' => '3ChezMoi', 'lieu_arrivee' => 'Metz', 'etat_besoin' => 'EnCours', 'type_vehicule' => 'Normal'));
        DB::table('besoin_transports')->insert(array('candidature_id' => 6, 'lieu_depart' => '3ChezMoi', 'lieu_arrivee' => 'Amsterdam', 'etat_besoin' => 'EnCours', 'type_vehicule' => 'Normal'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('besoin_transports');
    }
}
