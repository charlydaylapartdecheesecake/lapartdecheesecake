<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PropositionTransport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposition_transports', function (Blueprint $table) {
            $table->unsignedInteger('besoin_id');
            $table->foreign('besoin_id')->references('id')->on('besoin_transports')->onDelete('cascade');
            $table->unsignedInteger('offre_id');
            $table->foreign('offre_id')->references('id')->on('offre_transports')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('proposition_transports')->insert(array('offre_id' => 1, 'besoin_id' => 1));
        DB::table('proposition_transports')->insert(array('offre_id' => 2, 'besoin_id' => 2));
        DB::table('proposition_transports')->insert(array('offre_id' => 3, 'besoin_id' => 3));
        DB::table('proposition_transports')->insert(array('offre_id' => 4, 'besoin_id' => 4));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('porposition_transports');
    }
}
