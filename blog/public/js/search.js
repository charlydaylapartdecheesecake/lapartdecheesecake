$(function(){


	var offres;
	var selector="intitule";

	$.ajax({
		url: "offresJSON",
		method: "GET"
	}).done(function(result){
		offres = JSON.parse(result);
		for(offre of offres){
			$("#offres").append(offre.html);
		}
	});

	function reset(){
		console.log("-------")
		$("#offres").html("");
		search = $("#search").val();
		console.log(selector)
		for(offre of offres){
			let s = "";
			switch(selector){
				case 'intitule':
					s = offre.intitule;
					break;
				case 'description':
					s = offre.description;
					break;
				case 'categorie':
					s = offre.categorie;
					break; 
				case 'employeur':
					s = offre.employeur;
					break; 
			}

			console.log(s)
			if(s.toUpperCase().includes(search.toUpperCase()))
				$("#offres").append(offre.html);
		}
	}

	$(".radio").on( "click", function() {
	  selector = $( "input:checked" ).val()
	  reset();
	});

	$("#search").on('input', function(){
		reset();
	});

})
