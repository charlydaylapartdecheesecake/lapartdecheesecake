
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});

$(function(){



	var offres;

	$.ajax({
		url: "offresJSON",
		method: "GET"
	}).done(function(result){
		console.log(result);
		offres = JSON.parse(result);
		for(offre of offres){
			$("#offres").append(offre.html);
		}
	});

	$("#search").change(function(){
		alert("test");
		search = $("#search").text();
		res = "";
		for(offre of offres){
			if(offres.intitule.includes(search))
				res+=offre.html;
		}
		$("#offres").html(res);
	});

})

alert("test")