@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading"><a style="font-size: 16px">Se connecter</a></div>

                    <div class="panel-body">
                        <br>
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-2 control-label">E-Mail Address</label>

                                <div class="col-md-9">
                                    <input id="email" type="email" class="form-control form-control-lg mt-1" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-2 control-label">Password</label>

                                <div class="col-md-9">
                                    <input id="password" type="password" class="form-control form-control-lg mt-1" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-2">
                                    <div class="row justify-content-start">
                                        <div class="checkbox ml-4">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}><a style="font-size: 14px">Remember Me</a>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row justify-content-end">

                                            <a class="btn btn-link" href="{{ route('password.request') }}" style="font-size: 14px">
                                                Mot de passe oublie ?
                                            </a>
                                    </div>
                                    <button type="submit" class="btn btn-outline-primary btn-lg btn-block">
                                        Se connecter
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
