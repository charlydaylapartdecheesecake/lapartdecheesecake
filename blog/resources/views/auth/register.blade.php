@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading"><a style="font-size: 16px">S'enregistrer</a></div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nom</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control form-control-lg mt-1" name="name"
                                       value="{{ old('name') }}" required autofocus placeholder="Exemple : Coquelicot">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                            <label for="first_name " class="col-md-4 control-label">Prenom</label>
                            <div class="col-md-8">
                                <input id="first_name" type="text" class="form-control form-control-lg mt-1"
                                       name="first_name" value="{{ old('first_name') }}" required autofocus
                                       placeholder="Exemple : Alex">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Adresse Email</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control form-control-lg mt-1" name="email"
                                       value="{{ old('email') }}" required
                                       placeholder="Exemple : alex.coquelicot@hotmail.com">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('adressePerso') ? ' has-error' : '' }}">
                            <label for="adressePerso" class="col-md-4 control-label">Adresse personnelle</label>

                            <div class="col-md-8">
                                <input id="adressePerso" type="text" class="form-control form-control-lg mt-1"
                                       name="adressePerso" value="{{ old('adressePerso') }}"
                                       placeholder="Exemple : 32 rue des lilas">

                                @if ($errors->has('adressPerso'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('adressePerso') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('adressePro') ? ' has-error' : '' }}">
                            <label for="adressePro" class="col-md-4 control-label">Adresse professionelle</label>

                            <div class="col-md-8">
                                <input id="adressePro" type="text" class="form-control form-control-lg mt-1"
                                       name="adressePro" value="{{ old('adressePro') }}"
                                       placeholder="Exemple : 1 rue des roses">

                                @if ($errors->has('adressPro'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('adressePro') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Mot de passe</label>

                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control form-control-lg mt-1"
                                       name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmation du mot de passe</label>

                            <div class="col-md-8">
                                <input id="password-confirm" type="password" class="form-control form-control-lg mt-1"
                                       name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-outline-primary btn-lg btn-block">
                                    Valider
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
