@section('footbar')
    <nav class="navbar-expand-lg navbar-dark bg-dark foot">

        <!-- Branding Image -->
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <div class="container">
                <div class="row justify-content-center">
                <ul class="navbar-nav ml-5 mt-auto">
                    <li class="nav-item"><a class="nav-item nav-link ml-5" href="#">A propos</a></li>
                    <li class="nav-item"><a class="nav-item nav-link ml-5" href="#">Contact</a></li>
                    <li class="nav-item"><a class="nav-item nav-link ml-5" href="#">Plan</a></li>
                </ul>
                </div>
                </div>

            </div>
            </ul>
        </div>
        </div>
    </nav>

@endsection
