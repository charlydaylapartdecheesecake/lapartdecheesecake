@section('navbar')
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-static-top">

        <!-- Branding Image -->
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->

                    <a class="navbar-brand" href="{{ url('/') }}" style="font-size: 16px; padding-top: 1.9%">JustJobs</a>
                <ul class="navbar-nav ml-4 mt-auto">

                    <li class="nav-item"><a class="nav-item nav-link" href="{{route('listes_offres')}}">Offres
                            d'emploi</a></li>

                    @guest
                    @else
                        <li class="nav-item"><a class="nav-link" href="{{route('create_offer') }}">Creer une offre</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" id="navbarDropdownMenuLink" aria-expanded="false" aria-haspopup="true" style="font-size: 16px">Liste
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                              <ul>
                                  <li><a class="dropdown-item" href="{{route('liste_utilisateurs')}}">Liste des
                                          utilisateurs</a></li>

                                  <li><a class="dropdown-item" href="{{route('listes_candidatures_all')}}">Liste des
                                          candidatures à ses offres d'emploi</a></li>
                                  <li><a class="dropdown-item" href="{{route('listes_besoin_transport')}}">Liste des
                                          besoins de transport</a></li>
                              </ul>
                            </div>
                        </li>

                    @endguest
                </ul>

            </div>


                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <form class="form-inline">
                        <input class="form-control form-control-lg mr-sm-2" type="search" placeholder="Chercher une offre" aria-label="Search">
                        <button class="btn btn-outline-info btn-lg" type="submit">Recherche</button>
                    </form>

                    @guest
                        @if (Route::has('login'))
                            <div class="collapse navbar-collapse">
                                @auth
                                    <li class="nav-item"><a class="nav-item nav-link" href="{{ url('/home') }}">Home</a></li>
                                @else
                                    <li class="nav-item"><a class="nav-item nav-link" href="{{ url('/login') }}">Se connecter</a></li>
                                    <li class="nav-item"><a class="nav-item nav-link" href="{{ url('/register') }}">S'enregistrer</a></li>
                                @endauth
                            </div>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" id="navbarDropdownMenuLink" aria-expanded="false" aria-haspopup="true" style="font-size: 16px">
                                {{ \Auth::user()->name.' '.\Auth::user()->first_name }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('modifier-compte-form').submit();">
                                    Modifiez votre compte
                                </a>

                                <form id="modifier-compte-form" action="{{ route('modifier-compte') }}" method="GET"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>

                                <a class="dropdown-item" href="{{route('listes_mes_candidatures')}}">Mes candidatures</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Deconnexion
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    @endguest

                </ul>
            </div>
        </div>
    </nav>

@endsection
