@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
		<div class="col-md-10 col-md-offset-1">

            <div class="panel panel-primary">

				<div class="panel-heading">
					<a style="font-size: 16px">Candidater à {{$offre->intitule}}</a>

				</div>
				<div class="panel-body">

					<form method="POST" action="{{route('transport',array('id' => $offre->id))}}">
						{{ csrf_field() }}

						<div class="form-group">
							<label for="">Nécessite un besoin de transport?</label>
							<div class="btn-group btn-group-toggle">
								<label class="btn btn-outline-info active">
									<input class="radio" id="oui" type="radio" name="vehicule" id="option1" checked="checked" value="on" > Oui
								</label>
								<label class="btn btn-outline-info">
									<input class="radio" id="non" type="radio" name="vehicule" id="option2"  value="off"> Non
								</label>
							</div>

						</div>



						<div id="besoin-vehicule">
	                        <div class="form-group">
	                            <label for="">Type de véhicule</label>
	                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
	                                <label class="btn btn-outline-info active">
	                                    <input type="radio" name="typeVehicule" id="option1" autocomplete="off" value="normal" > Normal
	                                </label>
	                                <label class="btn btn-outline-info">
	                                    <input type="radio" name="typeVehicule" id="option2" autocomplete="off" value="adapte" > Adapté
	                                </label>
	                            </div>
	                        </div>

								<div class="form-group">
									<label for="depart">Lieu de départ :</label>
									<input type="text" class="form-control" id="depart" name="depart">
								</div>

	                    </div>



						<button type="submit" class="btn btn-outline-primary btn-lg btn-block">Valider</button>


					</form>

	            </div>

            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/candidater.js') }}"></script>
@endsection
