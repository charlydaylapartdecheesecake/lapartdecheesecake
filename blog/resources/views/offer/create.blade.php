@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-primary">

				<div class="panel-heading">
					<a style="font-size: 16px">Création d'une offre d'emploi</a>
				</div>

				<div class="panel-body">

	            	<form method="POST" action="{{ route('create_offer') }}">
	            		{{ csrf_field() }}
	            		<div class="form-group">
	            			<label for="">Intitulé</label>
	            			<input type="text" class="form-control" id="intitule" name="intitule">
	            		</div>

	            		<div class="form-group">
	            			<label for="">Description</label>
	            			<input type="text" class="form-control" id="description" name="description">
	            		</div>

	            		<div class="form-group">
	            			<label for="">Durée</label>
	            			<input type="text" class="form-control" id="duree" name="duree">
	            		</div>

	            		<div class="form-group">
	            			<label for="">Adresse</label>
	            			<input type="text" class="form-control" id="adresse" name="adresse">
	            		</div>


	            		<div class="form-group">
	            			<label for="">Categorie</label>
	            			<select id="cate_type" name="cate_type" class="form-control">
	            				<option selected="selected" disabled="disabled">Selectionnez une catégorie</option>
			            		@foreach($categories as $cate)
								    <option value="{{$cate->id}}">{{ $cate->type." : ".$cate->poste }}</option>
								@endforeach
	            			</select>
	            		</div>



						<button type="submit" class="btn btn-outline-primary btn-lg btn-block">Valider</button>


	            	</form>
	            </div>

            </div>
        </div>
    </div>
</div>

@endsection
