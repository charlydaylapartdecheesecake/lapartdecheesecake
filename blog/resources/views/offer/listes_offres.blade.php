@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading"><a style="font-size: 16px">Offres d'emploi</a></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col">
                                <input class="form-control form-control-lg" type="text" id="search">
                            </div>

                            <div class="btn-group btn-group-toggle">
                                    <label class="btn btn-outline-info active">
                                        <input class="radio" type="radio"  value="intitule" name="selector" checked="checked">Intitulé
                                    </label>

                                        <label class="btn btn-outline-info">
                                            <input class="radio" type="radio" value="description" name="selector">Description
                                        </label>

                                        <label class="btn btn-outline-info">
                                            <input class="radio" type="radio" value="categorie" name="selector">Catégorie
                                        </label>

                                        <label class="btn btn-outline-info mr-4">
                                            <input class="radio" type="radio" value="employeur" name="selector">Employeur
                                        </label>
                                </div>
                            </div>
                        </div>

                        <div id="offres">

                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <script src="{{ asset('js/search.js') }}"></script>
@endsection
