@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading"><a style="font-size: 16px">Offre d'emplois</a></div>

                    <div class="panel-body">

                            <div class="card border-primary text-center">
                                <div class="card-header"><h2>Intitule de l'offre : {{ $offre->intitule }}</h2></div>
                                <div class="card-body">
                                    <p style="font-size: 14px">description : {{ $offre->description }}</p>
                                    <p style="font-size: 14px">Durée du contrat : {{$offre->duree}} semaines</p>
                                    <p style="font-size: 14px">Adresse du lieu de travail : {{$offre->adresse}}</p>
                                    <p style="font-size: 14px">Catégorie de l'offre d'emplois : {{$offre->categorie()->type}}</p>

                                </div>
                                <div class="card-footer bg-transparent border-primary">
                                    <a class="btn btn-primary btn-lg btn-block mt-3 mb-3" href="{{$route}}">
                                        Postuler à cette offre
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
