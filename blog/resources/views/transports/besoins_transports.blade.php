@extends('layouts.app')

@section('content')

	<div class="container">

        <div>
		@foreach($besoins as $besoin)
            @if($besoin->etat_besoin!=="Complet")
                    <div class="list-groupe">
                        <a class="list-group-item list-group-item-action list-group-item-primary" >
                            <div class="d-flex w-100 justify-content-between">
                                <h2 class="mb-1" >Départ :{{$besoin->lieu_depart}}</h2>
                                <h2 class="mb-1" >Destination :{{$besoin->lieu_arrivee}}</h2>
                                <small class="mb-1" >{{$besoin->etat_besoin}}</small>
                            </div>
                            <p class="mb-1"> Type de véhicule demandé :{{$besoin->type_vehicule}}</p>


                            <form method="GET" action="{{ route('postuler_besoin', ["id"=>$besoin->id]) }}">

                                {{ csrf_field() }}
                                <div class="form-group">
                                    <button type="submit" class="btn btn-lg btn-primary" id="postuler" name="postuler">Postuler</button>
                                    <input name="idbesoin" value="{{$besoin->id}}" hidden="hidden">
                                </div>


                            </form>

                        </a>
                        <br>
                    </div>
                @endif


		@endforeach
        </div>
	</div>
@endsection