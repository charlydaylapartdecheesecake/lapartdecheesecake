@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        Liste de toutes les candidatures
                    </div>

                    <div class="panel-body">
                        @foreach($candidatures as $candidature)
                            <div class="card" style="width: 18rem;">
                                <!--<img class="card-img-top" src="..." alt="Card image cap">-->
                                <div class="card-body">
                                    <h5 class="card-title">{{ $candidature->intitule}}</h5>
                                    <p class="card-text">{{\User::find($candidature->candidat_id)->name}} {{\User::find($candidature->candidat_id)->first_name}}</p>

                                    <select id="etat" name="etat" class="form-control">
                                        <option selected="selected" disabled="disabled">Selectionnez une catégorie
                                        </option>

                                    </select>

                                    <a href="{{route('show_user',array('id' => $candidature->candidat_id))}}"
                                       class="btn btn-primary">Fiche utilisateur</a>
                                    <a href="{{route('show_offre',array('id' => $candidature->offre_id))}}"
                                       class="btn btn-danger">Offre d'emploi</a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
