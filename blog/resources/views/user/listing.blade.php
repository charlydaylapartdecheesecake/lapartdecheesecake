@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-primary">

                    <div class="panel-heading">
                        <a style="font-size: 16px">Liste des utilisateurs</a>
                    </div>

                    <div class="panel-body">
                        <div class="card-columns ml-5">

                        @foreach($users as $user)
                            <div class="card border-primary ml-5" style="width: 18rem;">
                                <div class="card-header"><h4>{{$user->name}} {{$user->first_name}}</h4></div>
                                <div class="card-body">
                                    <div class="front"><img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive"/></div>
                                    <div class="flip-back-card">
                                        <cite title="adresse_perso" class="mt-5"><i class="glyphicon glyphicon-map-marker"></i>{{$user->adresse_perso}}</cite>
                                        <br>
                                        <cite title="adresse_pro"><i class="glyphicon glyphicon-map-marker"></i><{{$user->adresse_pro}}</cite>
                                        <p><i class="glyphicon glyphicon-envelope"></i>{{$user->email}}</p>
                                    </div>
                                </div>
                            </div>
                                    <!--<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="{{route('show_user',array('id' => $user->id))}}" class="btn btn-primary">Fiche
                                        utilisateur</a>-->

                        @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
