@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading"><a style="font-size: 16px">Mes Candidatures</a></div>
                    <div class="panel-body">
                        @foreach($offres as $offre)
                            <div class="list-groupe ml-4 mr-4">
                                <a href="$route" class="list-group-item list-group-item-action list-group-item-primary">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h2 class="mb-1">$offre->intitule</h2>
                                        <small class="mb-1">$cate</small>
                                    </div>
                                    <p class="mb-1"> Description :$offre->description</p>
                                    <small class="mb-1"> Lieu :$offre->adresse</small>

                                </a>
                            </div>

                        @endforeach
                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
