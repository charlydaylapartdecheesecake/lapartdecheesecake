<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/modifier-compte', 'UserController@displayModif')->name('modifier-compte')->middleware('auth');
Route::get('/create', 'OfferController@displayCreateForm')->name('create_offer');
Route::post('/create', 'OfferController@registerOffer')->name('register_offer')->middleware('auth');
Route::get('/offres', 'OfferController@listes_offres')->name('listes_offres')->middleware('auth');
Route::get('/offre/{id}','OfferController@candidate')->name('show_offre')->middleware('auth');
Route::get('/offresJSON', 'OfferController@asJSON');
Route::get('/offre/{id}','OfferController@voir_offre')->name('show_offre')->middleware('auth');
Route::get('/offre/{id}/candidater','OfferController@candidate')->name('candidater')->middleware('auth');


Route::get('/liste/utilisateurs', 'UserController@listesUtilisateurs')->name('liste_utilisateurs')->middleware('auth'); //send user to auth page if not connected
Route::get('/user/{id}', 'UserController@show')->where('id', '[0-9]+')->name('show_user')->middleware('auth');
Route::get('/liste/toutes-les-candidatures', 'UserController@listesCandidatures')->name('listes_candidatures_all')->middleware('auth');
Route::get('/liste/mes-candidatures', 'UserController@listeMesCandidatures')->name('listes_mes_candidatures')->middleware('auth');

Route::post('/transport/{id}' , 'TransportController@besoinTransport')->name('transport')->middleware('auth');
Route::get('/transports','TransportController@listes_besoin_transport')->name('listes_besoin_transport')->middleware('auth');
Route::get('/transport/{id}/enregistrer','TransportController@postulerAuBesoin')->name('postuler_besoin')->middleware('auth');
